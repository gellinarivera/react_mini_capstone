import React from 'react';

//do this for each of the images
//import image from '../assets/images/image' 1000X600
import capstone1 from '../assets/images/capstone1.jpg';
import capstone2 from '../assets/images/capstone2.JPG';


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Card from '../components/Card';

class Carousel extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			items: [
				{
					id: 0,
					title: 'Static and Responsive Website',
					subTitle: 'Made with HTML, CSS, Bootstrap, and pictures used from Unsplash.',
					imgSrc: capstone1,
					link: 'https://gellinarivera.gitlab.io/capstone-1',
					selected: false
				},
				{
					id: 1,
					title: 'Course Booking Application',
					subTitle: 'Made with Bootstrap, NodeJS, MongoDB, and pictures used from Unsplash.',
					imgSrc: capstone2,
					link: 'https://tuitt.gitlab.io/students/batch68/gelina-rivera/s18_capstone_frontend',
					selected: false
				},
			]
		}
	}

		handleCardClick = (id, card) => {
			console.log(id);

			let items = [...this.state.items];

			items[id].selected = items[id].selected ? false : true;

			items.forEach(item => {
				if(item.id !== id) {
					item.selected = false;
				}
			});


			this.setState({
				items
			});
		}



		makeItems = (items) => {
			return items.map(item => {
				return <Card item={item} click={(e => this.handleCardClick(item.id, e))} key={item.id}/>
			});

		}


		render() {
			return(
				<Container fluid={true}>
					<Row className="justify-content-around">
						{this.makeItems(this.state.items)}
					</Row>
				</Container>
			);
		}
} 

export default Carousel;