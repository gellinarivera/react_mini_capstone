import React from 'react';

import Hero from '../components/Hero';
// import Content from '../components/Content';
import Carousel from '../components/Carousel';

import Container from 'react-bootstrap/Container';

function AboutPage(props){
	return(
		<div>
			<Hero className="text-center" title={props.title} />
			<hr className="w-50 mx-auto mb-5 mt-0" />
			<Container>
			<Carousel />
			</Container>
		</div>
	);
}

export default AboutPage;